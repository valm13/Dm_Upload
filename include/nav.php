<!-- Header -->
<header>
    <!--Navbar-->
    <nav>
        <div class="nav-wrapper">
            <!-- Logo centré -->
            <a href="index.php" id="navbar-logo" class="brand-logo center"><?php echo $GLOBALS['SITE_NAME'];?></a>


            <!-- Liens sur la gauche -->
            <ul id="nav-mobile" class="left hide-on-med-and-down">
                <!-- Liens habituels -->
                <?php
                if(!isset($_SESSION['membre']))
                {
                    $navbar_items = array
                (
                    'index.php' => '<i class="material-icons left">home</i>Accueil',
                ); 
                }
                else{
                    $navbar_items = array
                (
                    'files.php' => '<i class="material-icons left">folder</i>Fichiers',
                );
                }

                // Affiche les liens et ajoute la classe "active" pour la page courante
                foreach ($navbar_items as $key => $value)
                {
                    echo '<li' . (basename($_SERVER['SCRIPT_NAME']) == $key ? ' class="active">' : '>') . '<a href="' . $key . '">' . $value . '</a></li>';
                }
                ?>
            </ul>


            <!-- Liens sur la droite -->
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <!-- Liens habituels -->
                <?php

                    $navbar_items = array
                (

                );
                
                


                // Affiche les liens et ajoute la classe "active" pour la page courante
                foreach ($navbar_items as $key => $value)
                {
                    echo '<li' . (basename($_SERVER['SCRIPT_NAME']) == $key ? ' class="active">' : '>') . '<a href="' . $key . '">' . $value . '</a></li>';
                }
                ?>


                <!-- Liens conditionnels -->
                <?php
                // Si l'utilisateur est connecté
                if(isset($_SESSION['membre']))
                {
                    // Déconnexion
                    echo '<li' . (basename($_SERVER['SCRIPT_NAME']) == 'deconnexion.php' ? ' class="active">' : '>') .'<a href="deconnexion.php"><i class="material-icons">power_settings_new</i></a></li>';
                }
                // Si l'utilisateur n'est pas connecté
                else
                {
                    echo '<li' . (basename($_SERVER['SCRIPT_NAME']) == 'index.php' ? ' class="active">' : '>') .'<a href="index.php"><i class="material-icons left">vpn_key</i>Connexion</a></li>';
                }
                ?>
            </ul>
        </div>
    </nav>
</header>