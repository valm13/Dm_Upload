<?php
class Membre {
	// Caractéristiques du membre
	private $id_membre;						// ID du membre

	private $email;							// E-mail du compte
	private $mot_de_passe;					// Mot de passe du compte

	private $date_inscription;				// Date d'inscription

	private $nom;							// Nom
	private $prenom;						// Prénom


	/**
	*	[Constructeur] Initialise les variables selon le tableau de valeurs
	*	@version 30/12/2017 17:00
	*
	*	@param array $params Tableau de valeurs
	*	@return void
	*/
	public function __construct(array $params) {
		// On récupère les variables existantes de la classe
		$this_vars = get_object_vars($this);
		
		// On met en null par défaut chaque variable
		foreach ($this_vars as $key => $value)
		{
			$this->$key = null;
		}
		// Compte à confirmer par défaut
		$this->role = -1;


		// On modifie les variables avec les valeurs données
		$this->hydrate($params);
	}


	public function hydrate(array $params) {
		// On récupère les variables existantes de la classe
		$this_vars = get_object_vars($this);


		// On parcourt le paramètre
		foreach ($params as $key => $value)
		{
			// Si la clef existe en tant que variable de classe
			if (array_key_exists($key, $this_vars))
			{
				// La variable de classe prend la valeur donnée
				$this->$key = $value;
			}
		}
	}



	/*
	*	Getters
	*/
	
	/**
	*	Renvoie l'ID du membre
	*	@version 30/12/2017 17:00
	*
	*	@param void
	*	@return int ID
	*/
	public function id() {
		return $this->id_membre;
	}

	/**
	*	Renvoie l'e-mail du membre
	*	@version 30/12/2017 17:00
	*
	*	@param void
	*	@return string E-mail
	*/
	public function email() {
		return $this->email;
	}

	/**
	*	Renvoie la date d'inscription du membre
	*	@version 30/12/2017 17:00
	*
	*	@param void
	*	@return date Date d'inscription
	*/
	public function date_inscription() {
		return $this->date_inscription;
	}

	/**
	*	Renvoie le nom du membre
	*	@version 30/12/2017 17:00
	*
	*	@param void
	*	@return string Nom
	*/
	public function nom() {
		return $this->nom;
	}

	/**
	*	Renvoie le prénom du membre
	*	@version 30/12/2017 17:00
	*
	*	@param void
	*	@return string Prénom
	*/
	public function prenom() {
		return $this->prenom;
	}
	

	/**
	*	Renvoie le tableau de valeurs de l'objet
	*	@version 30/12/2017 17:00
	*
	*	@param void
	*	@return array Tableau de valeurs de l'objet instancié
	*/
	public function toArray() {
		$this_vars = get_object_vars($this);

		return $this_vars;
	}
}