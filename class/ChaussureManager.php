<?php
class ChaussureManager {
	// Informations BDD
	private $__host = 'localhost';				// Host de la BDD
	private $__dbname = 'instagram';			// Nom de la base
	private $__user = 'root';					// Nom d'utilisateur
	private $__pass = '';						// Mot de passe

	// Objet PDO
	private $_db;								// Objet PDO


	// Boutique
	private $_CHAMPS_MEMBRES;					// Liste des champs de la table 'membres'



	/**
	*	[Constructeur] Initialise la connexion selon le paramètre
	*	@version 30/12/2017 17:45
	*
	*	@param boolean $auto_connect Connexion automatique à la base de données
	*	@return void
	*/
	public function __construct($auto_connect) {
		// Si un booléen est passé en paramètre
		if (isset($auto_connect))
		{
			// Si ce booléen est TRUE, on se connecte directement à la BDD
			if ($auto_connect)
			{
				$this->connexionBDD();
			}
		}
	}



	/*
	*	GESTION DE LA BASE DE DONNÉES
	*/

	/**
	*	Connexion à la base de données
	*	@version 11/12/2017 18:00
	*
	*	@param void
	*	@return void
	*/
	private function connexionBDD() {
		try
		{
			// On essaie de se connecter à la BDD
			$this->_db = new PDO('mysql:host=' . $this->__host . ';dbname=' . $this->__dbname . ';charset=utf8', $this->__user, $this->__pass);

			// On initialise les variables de la classe
			$this->initialisation();
		}
		catch (PDOException $e)
		{
			echo 'Erreur connexion BDD : ';
			die($e->getMessage());
		}
	}


	/**
	*	Initialise les variables de classe
	*	@version 24/12/2017 18:00
	*
	*	@param void
	*	@return void
	*/
	private function initialisation() {

		/*  MEMBRES  */

		// On récupère les colonnes de la table 'membres'
		$req = $this->_db->query('SHOW COLUMNS FROM membres');
		
		// On parcourt les résultats
		$this->_CHAMPS_MEMBRES = array();
		foreach ($req->fetchAll() as $value)
		{
			// On ajoute le nom du champ dans le tableau
			$this->_CHAMPS_MEMBRES[] = $value['Field'];
		}
	}




	/*
	*	GESTION MEMBRE
	*/

	/**
	*	Vérifie les informations de connexion et procède à la connexion du membre si celles-ci sont correctes
	*	@version 11/12/2017 18:00
	*
	*	@param string $email E-mail du membre
	*	@param string $mot_de_passe Mot de passe du membre [en sha1]
	*	@return integer Indique l'état du compte
	*		0 : inexistant
	*		1 : connecté
	*		2 : à confirmer par e-mail
	*/
	public function connexion_membre($email, $mot_de_passe) {
		// On compte combien d'utilisateurs correspondent à ce couple (email, mot_de_passe)
		$req = $this->_db->prepare("SELECT * FROM membres WHERE email = :email AND mot_de_passe = :mot_de_passe");
		$req->bindParam(":email", $email);
		$req->bindParam(":mot_de_passe", $mot_de_passe);
		$req->execute();

		// On récupère les résultats
		$res = $req->fetchAll();

		// Si un utilisateur correspond à ce couple, c'est que l'authentification est réussie
		if (sizeof($res) == 1)
		{
			// Si l'e-mail a été confirmé

			// On copie les informations du compte dans la variable SESSION
			$_SESSION['membre'] = new Membre($res[0]);
			
			return 1; // Renvoie que la connexion est réussie
		}
		// Aucun utilisateur correspondant
		else {
			// On s'assure de déconnecter l'utilisateur
			unset($_SESSION['membre']);

			return 0; // Renvoie que la connexion a raté
		}
	}
    

    /**
	*	Indique si l'e-mail est déjà utilisé par un membre
	*	@version 11/12/2017 18:00
	*
	*	@param string $email
	*	@return boolean
	*		TRUE : E-mail déjà utilisé
	*		FALSE : E-mail non utilisé
	*/
    public function email_deja_utilise($email) {
    	// Préparation de la requête
        $req = $this->_db->prepare("SELECT id_membre FROM membres WHERE email = :email");
        $req->bindParam(":email", $email);
        $req->execute();

        // On récupère le résultat
        $res = $req->fetchAll();

        // Si le tableau de résultat est vide
        if (empty($res))
        {
            return false; // E-mail non utilisé
        }
        else
        {
            return true; // E-mail déjà utilisé
        }
    }


    /**
	*	Ajoute un nouveau membre dans la base de données
	*	@version 11/12/2017 18:00
	*
	*	@param array $values Tableau de valeurs du membre
	*	@return void
	*/
    public function ajoute_membre(array $values) {

    	// Format de datetime de mysql
    	$values['date_inscription'] = date("Y-m-d H:i:s");	

        // Préparation de la requête
        $req = $this->_db->prepare("INSERT INTO membres(id_membre,email, mot_de_passe, nom, prenom, date_inscription) VALUES(:id_membre,:email, :mot_de_passe, :nom, :prenom, :date_inscription)");

        // On rajoute les paramètres du tableau
        foreach ($this->_CHAMPS_MEMBRES as $key => $val)
        {
            if (isset($values[$val]))
            {
            	$req->bindValue(':'.$val, $values[$val]);

            }
            else
            {
            	$req->bindValue(':'.$val, null);
            }
        }

        // On exécute la requête (l'utilisateur est inscrit)

        return $req->execute();
    }


    public function modifie_membre(array $params) {
    	// Si les données sont vides
    	if (empty($params))
    	{
    		return false;
    	}


    	// Liste des champs à modifier
    	$champs = '';
    	foreach ($params as $key => $value)
    	{
    		if (in_array($key, $this->_CHAMPS_MEMBRES) && $key != 'id_membre')
    		{
    			$champs .= ' ' . $key . ' = :' . $key . ',';
    		}
    	}
    	$champs = substr($champs, 0, -1); // On enlève la dernière virugle

    	// Prépération de la requête
    	$req = $this->_db->prepare("UPDATE membres SET".$champs." WHERE id_membre = :id_membre");

    	// On rajoute les paramètres du tableau
    	foreach ($params as $key => $value)
    	{
    		if (in_array($key, $this->_CHAMPS_MEMBRES))
    		{
    			$req->bindValue(':'.$key, $value);
    		}
    	}

    	// Exécution de la requête
    	$req->execute();
    	// Mise à jour de la variable SESSION
    	$_SESSION['membre']->hydrate($params);

    	return true;
    }
	
	public function enregistreFichierBdd(array $values)
	{
        // Préparation de la requête
        $req = $this->_db->prepare("INSERT INTO files(appartient_a,nom_fichier,type_fichier,date_ajout) VALUES(:appartient_a,:nom_fichier,:type_fichier,:date_ajout)");

        // On rajoute les paramètres du tableau
        $req->bindParam(":appartient_a",$values['appartient_a']);
        $req->bindParam(":nom_fichier",$values['nom_fichier']);
        $req->bindParam(":type_fichier",$values['type_fichier']);
        $req->bindParam(":date_ajout",$values['date_ajout']);
        // foreach ($values as $key => $val)
        // {
        //     	$req->bindValue(':'.$val, $values[$val]);
        // }

        return $req->execute();
	}

	public function listeMesFichiers($id)
	{
		$req = $this->_db->prepare("SELECT * FROM files WHERE appartient_a = :id ORDER BY id_file DESC");
		$req->bindValue(':id', $id);
		$req->execute();
		return $req->fetchAll();
	}
}