<?php
require_once('util/Require.php');


// if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_FILES["userfile"]) && isset($_SESSION['membre'])) {
//     $target_dir = 'files/'.md5($GLOBALS['SALT'].$_SESSION['membre']->id());
//     $target_file = $target_dir . basename($_FILES["userfile"]["name"]);
//     $uploadOk = 1;
//     $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
//     if (file_exists($target_file)) {
//         echo "Sorry, file already exists.";
//         $uploadOk = 0;
//     }
//     if ($uploadOk == 0) {
//         echo "Sorry, your file was not uploaded.";
// // if everything is ok, try to upload file
//     } else {
//         if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $target_file)) {
//             echo "The file " . basename($_FILES["userfile"]["name"]) . " has been uploaded.";
//         } else {
//             echo "Sorry, there was an error uploading your file.";
//         }
//     }
// }
if(isset($_FILES['userfile']))
{
  // $dossier = 'upload/';
  $dossier = 'upload/'.md5($GLOBALS['SALT'].$_SESSION['membre']->id());
  // On crée le dossier de l'utilisateur s'il n'existe pas.
  if(!is_dir($dossier))
    mkdir($dossier);

  $fichier = basename($_FILES['userfile']['name']);
  $taille_maxi = 10000000; // 10 mo
  $taille = filesize($_FILES['userfile']['tmp_name']);
  $extensions = array('.png', '.gif', '.jpg', '.jpeg','.txt','.PNG','.php','.html');
  $extension = strtolower(strrchr($_FILES['userfile']['name'], '.')); // Je met en minuscule car rarement lex extensions sont en majuscules.
  //Début des vérifications de sécurité...
  if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
  {
       $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg, txt.';
  }
  if($taille>$taille_maxi)
  {
       $erreur = 'Le fichier est trop gros...';
  }
  if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
  {
       //On formate le nom du fichier ici...
       $fichier = strtr($fichier, 
            'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
            'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
       $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
       if($extension == '.php' || $extension == '.html')
       {
          if(move_uploaded_file($_FILES['userfile']['tmp_name'], $dossier .'/'. $fichier.'.old')) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
         {
            // on ajoute le fichier à la bdd
            $values = array(
              'appartient_a' => $_SESSION['membre']->id(),
              'nom_fichier' => $_FILES['userfile']['name'].'.old',
              'type_fichier' => $extension,
              'date_ajout' => date("Y-m-d H:i:s"),
            );
            if($extension == '.php'){}
            $man = new ChaussureManager(TRUE);
            $man->enregistreFichierBdd($values); 
           
        }
       }
       if(move_uploaded_file($_FILES['userfile']['tmp_name'], $dossier .'/'. $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
       {
            // on ajoute le fichier à la bdd
            $values = array(
              'appartient_a' => $_SESSION['membre']->id(),
              'nom_fichier' => $_FILES['userfile']['name'],
              'type_fichier' => $extension,
              'date_ajout' => date("Y-m-d H:i:s"),
            );
            if($extension == '.php'){$values['nom_fichier'] = $values['nom_fichier'].'.old';}
            $man = new ChaussureManager(TRUE);
            $man->enregistreFichierBdd($values); 
           
       }
       else //Sinon (la fonction renvoie FALSE).
       {
            $erreur = 'Echec de l\'upload !';
            echo 'Echec de l\'upload !';
       }
  }
  else
  {
       echo $erreur;
  }
}

?>
<html>
    <head>
        <title>File Upload Progress Bar</title>
        <link rel="stylesheet" type="text/css" href="progress.css">
    </head>
    <body>
        <div id="bar_blank">
            <div id="bar_color"></div>
        </div>
        <div id="status"></div>
        <form action="form.php" method="post" 
              id="myForm" enctype="multipart/form-data" target="hidden_iframe">
            <input type="hidden" value="myForm"
                   name="<?php echo ini_get("session.upload_progress.name"); ?>">
            <input type="file" name="userfile"><br><br>
            <input type="submit" value="Start Upload">
        </form>
        <iframe id="hidden_iframe" name="hidden_iframe" src="about:blank" style="display: none;"></iframe>
        <script type="text/javascript">
            function toggleBarVisibility() {
                var e = document.getElementById("bar_blank");
                e.style.display = (e.style.display == "block") ? "none" : "block";
            }


            function sendRequest() {
                var XHR = new XMLHttpRequest();
                XHR.onreadystatechange = function () {
                    var response;
                if (XHR.readyState == 4) {
                    response = XHR.responseText;
                    document.getElementById("bar_color").style.width = response + "%";
                    document.getElementById("status").innerHTML = response + "%";

                    if (response < 100) {
                        setTimeout("sendRequest()", 10);
                    } else {
                        toggleBarVisibility();
                        document.getElementById("status").innerHTML = "Fait.";
                    }
                }
                };
                XHR.open("GET", "progress.php");
                XHR.send(null);
            }


            function startUpload() {
                toggleBarVisibility();
                setTimeout("sendRequest()", 10);
            }

            (function () {
                document.getElementById("myForm").onsubmit = startUpload;
            })();
        </script>
    </body>
</html>