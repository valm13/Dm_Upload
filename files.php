<?php
// Gère les dépendances de la page
require_once('util/Require.php');
if(!isset($_SESSION['membre']))
    header('Refresh:5; url=index.php');    // Redirection au bout de 5 secondes
/*
*   FORMULAIRE [POST] - Envois de fichier
*/

// Valeurs nécessaires à la validation du formulaire de connexion
$formulaire = array(
    'email' => '',
    'mot_de_passe' => ''
);
?>

<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize/sass/materialize.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />


    <!-- Encodage et favicon -->
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="img/favicon.png" sizes="128x128" />

    <!-- Feuilles de style -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <!-- Titre de la page -->
    <title><?php echo $GLOBALS['SITE_NAME'];?> > Fichiers</title>

</head>

<body>
    <!--Navbar-->
    <?php include('include/nav.php'); ?>


    <!--Main-->
    <main>

        <!-- PAS CONNECTE -->
        <?php 
        if(!isset($_SESSION['membre'])){
            ?>
            <!-- Titre du contenu -->
            <h1 class="center align">Vous n'êtes pas connecté</h1>
            <div class="row">
                <div class="col s6 offset-s3 center-align">
                    <!-- Carte -->
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <!-- Contenu de la carte -->
                            <div class="card-content">
                                <p class="center-align">
                                    Redirection automatique vers la page d'accueil.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
        else{
        ?>
        <!-- ON EST BIEN CONNECTE -->
        <div class="row">
            <div class="col s6 offset-s3 center-align">
                <!-- Carte -->
                <div class="card horizontal">
                    <div class="card-stacked">
                        <!-- Contenu de la carte -->
                        <div class="card-content">
                            <p class="center-align">
                                Je te laisse gérer tes fichiers <?php echo $_SESSION['membre']->prenom().' '.$_SESSION['membre']->nom();?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- UPLOAD -->
        <div class="row">
            <div class="col s4 offset center-align">
                <!-- Carte -->
                <div class="card horizontal">
                    <div class="card-stacked">
                        <!-- Contenu de la carte -->
                        <div class="card-content">
                            <h2 class="center-align">
                                Upload
                            </h2>
                            <?php require_once('form.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s5 offset-s2">
                <ul >
                        
                        <?php
                        $manager = new ChaussureManager(TRUE);
                        $toutFichiers = $manager->listeMesFichiers($_SESSION['membre']->id());
                        $dossier = 'upload/'.md5($GLOBALS['SALT'].$_SESSION['membre']->id());
                        // On parcourt la liste des items
                        foreach ($toutFichiers as $key => $value)
                        {
                        ?>
                        
                        <!-- Nouvelle ligne d'article -->
                        <li>
                            <div class="row">
                                <div class="col s6">
                                    <p>
                                        <a href=<?php echo '"'.$dossier.'/'.$value['nom_fichier'].'" download'?>>
                                            <?php echo $value['nom_fichier']; ?>        
                                        </a>
                                    </p>

                                </div>

                                    <?php } ?>
                            </div>
                        </li>
                        <?php } ?>
                </ul>
            </div>
        </div>

        


    </main>


    <!--Importation de JQuery avant materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="css/materialize/js/materialize.min.js"></script>
</body>
</html>
